import React from 'react';

const Daily = ({ c, r, d, f }) => {
  return (
    <div className='d-flex justify-content-around mt-3 mx-5 mb-5'>
        <div>
            <h1 className='fs-4'>Confirmed Cases</h1>
            <h3>{f(c)}</h3>
        </div>
        <div>
            <h1 className='fs-4'>Recovered Cases</h1>
            <h3>{f(r)}</h3>
        </div>
        <div>
            <h1 className='fs-4'>Deaths</h1>
            <h3>{f(d)}</h3>
        </div>
    </div>
  )
}

export default Daily;