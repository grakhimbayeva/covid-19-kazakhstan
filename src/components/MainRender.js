import React from "react";

const MainRender = ({ 
    renderNumbers, 
    totalConfirmed, totalDeaths, totalRecovered, 
    setFromDate, setToDate, 
    handleSearch, 
    dailyData, 
    lastDay, 
    head, upd,
    totalC, totalR, totalD,
    srch, srchHead,
    casesC, casesR, casesD }) => {
        return (
            <>
                <div className='justify-content-center mt-5'>
                    <h1 className='fs-2 text-uppercase'>{head}</h1>
                    <h3 className='fs-6 fst-italic text-white opacity-100'>{upd}: {lastDay}</h3>
                </div>
                <div className='d-flex my-5 justify-content-around'>
                    <div className=''>
                        <h1 className='fs-5'>{totalC}</h1>
                        <h2>{renderNumbers(totalConfirmed)}</h2>
                    </div>
                    <div className=''>
                        <h1 className='fs-5'>{totalR}</h1>
                        <h2>{renderNumbers(totalRecovered)}</h2>
                    </div>
                    <div className=''>
                        <h1 className='fs-5'>{totalD}</h1>
                        <h2>{renderNumbers(totalDeaths)}</h2>
                    </div>
                </div>
                <div className='mb-5 input-group w-50 align-self-center'>
                    <input
                        className='form-control' 
                        type='date' 
                        placeholder='from' 
                        onChange={(e) => setFromDate(e.target.value)}
                    />
                    <input
                        className='form-control' 
                        type='date' 
                        placeholder='to' 
                        onChange={(e)=> setToDate(e.target.value)}
                    />
                    <button className="btn btn-info" type='submit' onClick={handleSearch}>
                        {srch}
                    </button>
                </div>
                <div className='range'>
                    <h2 className='fs-6 text-white opacity-100'>{srchHead}</h2>
                </div>
                <div className='d-flex justify-content-around mt-3 mx-5 mb-5'>
                    <div>
                        <h1 className='fs-4'>{casesC}</h1>
                        <h3>{renderNumbers(dailyData.con)}</h3>
                    </div>
                    <div>
                        <h1 className='fs-4'>{casesR}</h1>
                        <h3>{renderNumbers(dailyData.rec)}</h3>
                    </div>
                    <div>
                        <h1 className='fs-4'>{casesD}</h1>
                        <h3>{renderNumbers(dailyData.dea)}</h3>
                    </div>
                </div>
            </> 
        )
};

export default MainRender;