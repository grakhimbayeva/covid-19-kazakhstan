import React, { useState, useEffect } from 'react';
import { Routes, Route, Link } from 'react-router-dom';
import MainRender from './MainRender';


const Main = () => {
    const [ confirmed, setConfirmed ] = useState([]);
    const [ recovered, setRecovered ] = useState([]);
    const [ deaths, setDeaths ] = useState([]);
    const [ fromDate, setFromDate ] = useState('2020-01-22');
    const [ toDate, setToDate ] = useState('2020-01-22');
    const [ dailyData, setDailyData ] = useState({
        con: 0,
        rec: 0,
        dea: 0
    });


    const getData = async() => {
        try {
        await fetch('https://raw.githubusercontent.com/bence-toth/covid-data/main/data/entire-dataset/kazakhstan.json')
            .then(response => response.json())
            .then(result => {
                const confirmedArr = result.dailyConfirmedCases;
                const recoveredArr = result.dailyRecoveredCases;
                const deathsArr = result.dailyDeaths;

                setConfirmed(confirmedArr);
                setRecovered(recoveredArr);
                setDeaths(deathsArr);
            });
        }
        catch(err){console.log(err)}
    }

    useEffect(()=>{
        getData();
    },[])

    const getLastDay = (i) => {
        const firstDate = new Date('2020-01-22');
        const date = new Date(firstDate.setDate(firstDate.getDate() + confirmed.length-i))
        return date;
    }

    const totalConfirmed = parseInt(confirmed.reduce((a,b)=> a+b, 0));
    const totalRecovered = parseInt(recovered.reduce((a,b)=> a+b, 0));
    const totalDeaths = parseInt(deaths.reduce((a,b)=> a+b, 0));
    const preLastDay = getLastDay(0).toDateString()
    const lastDay = preLastDay.split(' ')[2] + ' ' + preLastDay.split(' ')[1] + ' ' + preLastDay.split(' ')[3]

    const renderNumbers = (num) => {
        const formattedNum = new Intl.NumberFormat(['ban', 'id']).format(num);
        return formattedNum.split('.').join(' ');
    }

    const countDays = (inputDate) => {
        const preDays = new Date(inputDate) - new Date('2020-01-22');
        const days = preDays / (1000 * 60 * 60 * 24);
        return days;
    }

    const handleSearch = () => {
        const from = countDays(fromDate);
        const to = countDays(toDate);
        setDailyData({
            con: confirmed.slice(from-1, to+1).reduce((a,b)=> a+b, 0),
            rec: recovered.slice(from-1, to+1).reduce((a,b)=> a+b, 0),
            dea: deaths.slice(from-1, to+1).reduce((a,b)=> a+b, 0),
        })
    }

    const dateRu = (inputDate) => {
        return new Intl.DateTimeFormat('ru-RU').format(new Date(inputDate));
    }

    return (
        <div className='m-5 vh-100% w-100% align-self-center justify-self-center text-center text-wrap d-flex flex-column bg-success bg-gradient bg-opacity-50 rounded shadow p-2'>
            <Routes>
                <Route path='/' element={
                    <MainRender
                        totalConfirmed={totalConfirmed}
                        totalRecovered={totalRecovered}
                        totalDeaths={totalDeaths}
                        lastDay={lastDay}
                        renderNumbers={renderNumbers}
                        setFromDate={setFromDate}
                        setToDate={setToDate}
                        dailyData={dailyData}
                        handleSearch={handleSearch}
                        head='Covid-19 Cases in Kazakhstan'
                        upd='Updated'
                        totalC='Total Confirmed Cases'
                        totalR='Total Recovered Cases'
                        totalD='Total Deaths'
                        srch='Search'
                        srchHead={`Covid-19 cases from ${fromDate} to ${toDate}`}
                        casesC='Confirmed Cases'
                        casesR='Recovered Cases'
                        casesD='Deaths' 
                    />
                } />
                <Route path='/ru' element={
                    <MainRender
                        totalConfirmed={totalConfirmed}
                        totalRecovered={totalRecovered}
                        totalDeaths={totalDeaths}
                        lastDay={dateRu(lastDay)}
                        renderNumbers={renderNumbers}
                        setFromDate={setFromDate}
                        setToDate={setToDate}
                        toDate={toDate}
                        fromDate={fromDate}
                        dailyData={dailyData}
                        handleSearch={handleSearch}
                        head='Статистика Covid-19 по Казахстану'
                        upd='Обновлено'
                        totalC='Всего заболело'
                        totalR='Всего выздоровело'
                        totalD='Всего умерло'
                        srch='Поиск'
                        srchHead={`Случаи Covid-19 за период с ${fromDate} по ${toDate}`}
                        casesC='Заболело'
                        casesR='Выздоровело'
                        casesD='Умерло'
                    />
                } />
                <Route path='/kz' element={
                    <MainRender
                        totalConfirmed={totalConfirmed}
                        totalRecovered={totalRecovered}
                        totalDeaths={totalDeaths}
                        lastDay={dateRu(lastDay)}
                        renderNumbers={renderNumbers}
                        setFromDate={setFromDate}
                        setToDate={setToDate}
                        toDate={toDate}
                        fromDate={fromDate}
                        dailyData={dailyData}
                        handleSearch={handleSearch}
                        head='Қазақстандағы Covid-19 статистикасы'
                        upd='Жаңартылған'
                        totalC='Барлығы жұқтырғандар'
                        totalR='Барлығы жазылғандар'
                        totalD='Барлығы қайтыс болғандар'
                        srch='Іздеу'
                        srchHead={`${fromDate} по ${toDate} кезеңіндегі Covid-19 жағдайлары`}
                        casesC='Жұқтырғандар'
                        casesR='Жазылғандар'
                        casesD='Қайтыс болғандар' 
                    />
                } />
            </Routes>
            <div className='m-2'>
                <Link to='/' className='p-1 m-1 btn btn-info shadow opacity-75'>EN</Link>
                <Link to='/ru' className='p-1 m-1 btn btn-info shadow opacity-75'>RU</Link>
                <Link to='/kz' className='p-1 m-1 btn btn-info shadow opacity-75'>KZ</Link>
            </div>
        </div>
    );
}

export default Main;