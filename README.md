COVID-19 Statistics in Kazakhstan

## Description
This site will help you to reach COVID-19 statistics in Kazakhstan up to date in no time. You can see total confirmed, recovery and death cases for now and search for numbers in particular period as well.

## Visuals
![Alt text](image-1.png)

Searching calendar
![Alt text](image.png)

KZ version
![Alt text](image-2.png)

## Used tools
React js (useState, React Router)
Bootstrap
REST API
CSS
HTML
